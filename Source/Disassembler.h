#include "C8Decoder.h"

#include <cstdio>
#include <vector>

class Disassembler : protected C8Decoder<Disassembler>
{
	friend struct C8Decoder<Disassembler>;

	FILE* Out;

	void SYS(uint16_t syscall);
	void CLS();
	void RET();

	// 12-bit immediate
	void JP(uint16_t address);
	void CALL(uint16_t address);
	void LD_Immediate(uint8_t reg, uint8_t immediate);
	void ADD_Immediate(uint8_t reg, uint8_t immediate);
	void LD_Address(uint16_t immediate);
	void JP_V0(uint16_t rel_address);

	// 8-bit immediate
	void SE_Immediate(uint8_t reg, uint8_t val);
	void SNE_Immediate(uint8_t reg, uint8_t val);

	// Two-reg operations
	void SE_Reg(uint8_t reg_x, uint8_t reg_y);
	void SNE_Reg(uint8_t reg_x, uint8_t reg_y);
	void LD_Vx_Vy(uint8_t target_reg, uint8_t source_reg);
	void OR(uint8_t target_reg, uint8_t source_reg);
	void AND(uint8_t target_reg, uint8_t source_reg);
	void XOR(uint8_t target_reg, uint8_t source_reg);
	void ADD(uint8_t target_reg, uint8_t source_reg);
	void SUB(uint8_t target_reg, uint8_t source_reg);
	void SHR(uint8_t target_reg, uint8_t source_reg);
	void SUBN(uint8_t target_reg, uint8_t source_reg);
	void SHL(uint8_t target_reg, uint8_t source_reg);

	// One register operations
	void SKP(uint8_t reg);
	void SKNP(uint8_t reg);
	void LD_Vx_DT(uint8_t reg);
	void LD_Vx_K(uint8_t reg);
	void LD_DT_Vx(uint8_t reg);
	void LD_ST_Vx(uint8_t reg);
	void ADD_I_Vx(uint8_t reg);
	void LD_F_Vx(uint8_t reg);
	void LD_B_Vx(uint8_t reg);
	void LD_I_Vx(uint8_t reg);
	void LD_Vx_I(uint8_t reg);

	// Special
	void RND(uint8_t reg, uint8_t mask);
	void DRW(uint8_t reg_x, uint8_t reg_y, uint8_t sprite_size);

public:
	void Disassemble(const std::vector<uint8_t>& code, FILE* out);
};
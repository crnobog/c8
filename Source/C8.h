#pragma once

#include "C8Decoder.h"

#include <array>
#include <cstdint>
#include <vector>

struct C8 : private C8Decoder<C8>
{
	static const unsigned MemWriteMin = 0x200;
	static const unsigned MemorySize = 4096;
	static const unsigned NumRegisters = 16;
	static const unsigned StackSize = 16;
	static const unsigned DisplayWidth = 64;
	static const unsigned DisplayHeight = 32;
	static const unsigned DisplaySize = DisplayWidth * DisplayHeight / 8;
	static const unsigned NumKeys = 16;
	static const unsigned NumSprites = 16;
	static const unsigned BytesPerSprite = 5;

	static const uint8_t C8::Sprites[NumSprites][BytesPerSprite];

	std::array<uint8_t, MemorySize>		Memory;
	std::array<uint8_t, NumRegisters>	V;
	std::array<uint16_t, StackSize>		Stack;
	std::array<uint8_t, DisplaySize>	Display;
	std::array<bool, NumKeys>			KeyState;
	uint16_t	I, PC;
	uint8_t		SP, DT, ST;

	bool	WaitingForKey;
	uint8_t KeyTargetReg;

	C8(const std::vector<uint8_t>& rom);

	void ResetMachine(void);
	bool KeyDown(uint8_t key);
	bool GetDisplay(uint8_t x, uint8_t y);
	void Step(void);

protected:
	friend struct C8Decoder<C8>;

	void SYS(uint16_t syscall);
	void CLS();
	void RET();

	// 12-bit immediate
	void JP(uint16_t address);
	void CALL(uint16_t address);
	void LD_Immediate(uint8_t reg, uint8_t immediate);
	void ADD_Immediate(uint8_t reg, uint8_t immediate);
	void LD_Address(uint16_t immediate);
	void JP_V0(uint16_t rel_address);

	// 8-bit immediate
	void SE_Immediate(uint8_t reg, uint8_t val);
	void SNE_Immediate(uint8_t reg, uint8_t val);

	// Two-reg operations
	void SE_Reg(uint8_t reg_x, uint8_t reg_y);
	void SNE_Reg(uint8_t reg_x, uint8_t reg_y);
	void LD_Vx_Vy(uint8_t target_reg, uint8_t source_reg);
	void OR(uint8_t target_reg, uint8_t source_reg);
	void AND(uint8_t target_reg, uint8_t source_reg);
	void XOR(uint8_t target_reg, uint8_t source_reg);
	void ADD(uint8_t target_reg, uint8_t source_reg);
	void SUB(uint8_t target_reg, uint8_t source_reg);
	void SHR(uint8_t target_reg, uint8_t source_reg);
	void SUBN(uint8_t target_reg, uint8_t source_reg);
	void SHL(uint8_t target_reg, uint8_t source_reg);

	// One register operations
	void SKP(uint8_t reg);
	void SKNP(uint8_t reg);
	void LD_Vx_DT(uint8_t reg);
	void LD_Vx_K(uint8_t reg);
	void LD_DT_Vx(uint8_t reg);
	void LD_ST_Vx(uint8_t reg);
	void ADD_I_Vx(uint8_t reg);
	void LD_F_Vx(uint8_t reg);
	void LD_B_Vx(uint8_t reg);
	void LD_I_Vx(uint8_t reg);
	void LD_Vx_I(uint8_t reg);

	// Special
	void RND(uint8_t reg, uint8_t mask);
	void DRW(uint8_t reg_x, uint8_t reg_y, uint8_t sprite_size);

	uint16_t Read16(uint16_t address);
	void	Store8(uint16_t address, uint8_t val);
	uint8_t Read8(uint16_t address);
};

#include "Assembler.h"

#include <fstream>
#include <string>

using std::string;

string TestProgram = 
"LD V0, 0\n"
"LD V1, 0x0\n"
"LD V2, 0\n"
"loophead:\n"
"LD F, V2			# draw character\n"
"DRW V0, V1, 0x5\n"
"ADD V0, 0x5\n"
"ADD V2, 0x1\n"
"SNE V2, 0xF\n"
"JP halt\n"
"SE V0, 0x3C			# 12 chars\n"
"JP loophead\n"
"LD V0, 0\n"
"ADD V1, 0x6			# new line\n"
"JP loophead\n"
"halt:\n"
"JP halt\n"
;


int main(int argc, char** argv)
{
	string program;
	if (argc > 1)
	{

	}
	else
	{
		program = TestProgram;
	}
	
	if (program.size() == 0)
	{
		return 1;
	}

	Assembler ass;
	if (ass.Assemble(program))
	{
		auto code = ass.GetCode();
		return 0;
	}
	else
	{
		return 1;
	}
}

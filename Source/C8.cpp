#include "C8.h"


static bool MaskExact(uint16_t inst, uint16_t mask)
{
	return (inst & mask) == mask;
}

const uint8_t C8::Sprites[C8::NumSprites][C8::BytesPerSprite] = {
	{ 0xF0, 0x90, 0x90, 0x90, 0xF0 }, // 0
	{ 0x20, 0x60, 0x20, 0x20, 0x70 }, // 1
	{ 0xF0, 0x10, 0xF0, 0x80, 0xF0 }, // 2
	{ 0xF0, 0x10, 0xF0, 0x10, 0xF0 }, // 3
	{ 0x90, 0x90, 0xF0, 0x10, 0x10 }, // 4
	{ 0xF0, 0x80, 0xF0, 0x10, 0xF0 }, // 5
	{ 0xF0, 0x80, 0xF0, 0x90, 0xF0 }, // 6
	{ 0xF0, 0x10, 0x20, 0x40, 0x40 }, // 7
	{ 0xF0, 0x90, 0xF0, 0x90, 0xF0 }, // 8
	{ 0xF0, 0x90, 0xF0, 0x10, 0xF0 }, // 9
	{ 0xF0, 0x90, 0xF0, 0x90, 0x90 }, // A
	{ 0xE0, 0x90, 0xE0, 0x90, 0xE0 }, // B
	{ 0xF0, 0x80, 0x80, 0x80, 0xF0 }, // C
	{ 0xE0, 0x90, 0x90, 0x90, 0xE0 }, // D
	{ 0xF0, 0x80, 0xF0, 0x80, 0xF0 }, // E
	{ 0xF0, 0x80, 0xF0, 0x80, 0x80 }, // F
};

C8::C8(const std::vector<uint8_t>& rom) 
	: I(0)
	, PC(0x200)
	, SP(0xFF)
{
	Memory.fill(0);
	V.fill(0);
	Stack.fill(0);
	Display.fill(0);
	KeyState.fill(0);

	for (int sprite = 0; sprite < NumSprites; ++sprite)
	{
		memcpy_s(Memory.data() + sprite * BytesPerSprite, MemorySize - sprite*BytesPerSprite, Sprites[sprite], BytesPerSprite);
	}

	memcpy_s(Memory.data() + MemWriteMin, MemorySize - MemWriteMin, rom.data(), rom.size());
}

void C8::ResetMachine(void)
{
	PC = 0x200;
	SP = 0;
	I = 0;
	V.fill(0);
}

void C8::Step(void)
{
	uint16_t inst = Read16(PC);
	PC += 2;

	C8Decoder::Decode(inst);
}

void C8::LD_Vx_I(uint8_t reg)
{
	for (uint8_t i = 0; i < reg + 1; i)
	{
		V[i] = Read8(I + i);
	}
}

void C8::LD_I_Vx(uint8_t reg)
{
	for (uint8_t i = 0; i < reg + 1; i)
	{
		Store8(I + i, V[i]);
	}
}

void C8::LD_B_Vx(uint8_t reg)
{
	// store decimals of V[reg] into I, I+1, I+2
	uint8_t val = V[reg];
	uint8_t hundreds = val / 100;
	uint8_t tens = (val % 100) / 10;
	uint8_t ones = (val % 10);

	Store8(I, hundreds);
	Store8(I + 1, tens);
	Store8(I + 2, ones);
}

void C8::LD_F_Vx(uint8_t reg)
{
	// Load address of hex sprite V[reg] into I
	I = V[reg] * BytesPerSprite;
}

void C8::ADD_I_Vx(uint8_t reg)
{
	uint16_t new_val = I + V[reg];
	I = new_val;
}

void C8::LD_ST_Vx(uint8_t reg)
{
	ST = V[reg];
}

void C8::LD_DT_Vx(uint8_t reg)
{
	DT = V[reg];
}

void C8::LD_Vx_K(uint8_t reg)
{
	WaitingForKey = true;
	KeyTargetReg = reg;
}

void C8::LD_Vx_DT(uint8_t reg)
{
	V[reg] = DT;
}

void C8::SKNP(uint8_t reg)
{
	uint8_t val = V[reg];
	if (!KeyDown(val))
	{
		PC += 2;
	}
}

void C8::SKP(uint8_t reg)
{
	uint8_t val = V[reg];
	if (KeyDown(val))
	{
		PC += 2;
	}
}

void C8::DRW(uint8_t reg_x, uint8_t reg_y, uint8_t sprite_size)
{
	uint8_t orig_y = V[reg_y];
	uint8_t orig_x = V[reg_x];

	// Each line will be split into two bytes to be XOR'd into adjacent bytes of the display.
	uint8_t screen_x = orig_x / 8;
	// How much of the byte will be in screen_x + 1
	uint8_t right_amount = (orig_x % 8);
	uint8_t left_amount = 8 - right_amount;

	bool collision = false;
	for (uint8_t sprite_y = 0; sprite_y < sprite_size; ++sprite_y)
	{
		uint8_t screen_y = orig_y + sprite_y;
		if (screen_y >= DisplayHeight)
		{
			screen_y -= DisplayHeight;
		}
		const uint8_t sprite_byte = Read8(I + sprite_y);

		uint8_t right_byte = sprite_byte << left_amount;
		uint8_t left_byte = sprite_byte >> right_amount;

		uint8_t left_address = screen_y * ((uint8_t)DisplayWidth / 8) + screen_x;
		uint8_t right_address = left_address + 1;

		collision = collision || (Display[left_address] & left_byte) || (Display[right_address] & right_byte);

		Display[left_address] ^= left_byte;
		Display[right_address] ^= right_byte;


	}
	V[0xF] = collision ? 1 : 0;
}

void C8::RND(uint8_t reg, uint8_t mask)
{
	double r = (double)rand() / (double)(RAND_MAX + 1);
	uint8_t val = (uint8_t)(r * 256);
	V[reg] = val & mask;
}

void C8::JP_V0(uint16_t rel_address)
{
	uint16_t address = rel_address + PC;
	PC = address;
}

void C8::LD_Address(uint16_t immediate)
{
	I = immediate;
}

void C8::SNE_Reg(uint8_t reg_x, uint8_t reg_y)
{
	if (V[reg_x] != V[reg_y])
	{
		PC += 2;
	}
}

void C8::SE_Reg(uint8_t reg_x, uint8_t reg_y)
{
	if (V[reg_x] == V[reg_y])
	{
		PC += 2;
	}
}

void C8::SHL(uint8_t target_reg, uint8_t source_reg)
{
	V[0xF] = (V[target_reg] & 0x8000) != 0;
	V[target_reg] <<= 1;
}

void C8::SUBN(uint8_t target_reg, uint8_t source_reg)
{
	uint8_t lhs = V[source_reg];
	uint8_t rhs = V[target_reg];
	V[0xF] = (lhs > rhs);
	V[target_reg] = lhs - rhs;
}

void C8::SHR(uint8_t target_reg, uint8_t source_reg)
{
	V[0xF] = (V[target_reg] & 0x1);
	V[target_reg] >>= 1;
}

void C8::SUB(uint8_t target_reg, uint8_t source_reg)
{
	uint8_t lhs = V[target_reg];
	uint8_t rhs = V[source_reg];
	V[0xF] = (lhs > rhs);
	V[target_reg] = lhs - rhs;
}

void C8::ADD(uint8_t target_reg, uint8_t source_reg)
{
	uint32_t val = V[target_reg] + V[source_reg];
	V[target_reg] = val & 0xFF;
	V[0xF] = (val & 0xFF) != 0;
}

void C8::XOR(uint8_t target_reg, uint8_t source_reg)
{
	V[target_reg] ^= V[source_reg];
}

void C8::AND(uint8_t target_reg, uint8_t source_reg)
{
	V[target_reg] &= V[source_reg];
}

void C8::OR(uint8_t target_reg, uint8_t source_reg)
{
	V[target_reg] |= V[source_reg];
}

void C8::LD_Vx_Vy(uint8_t target_reg, uint8_t source_reg)
{
	V[target_reg] = V[source_reg];
}

void C8::ADD_Immediate(uint8_t reg, uint8_t immediate)
{
	V[reg] += immediate; // TODO: Check truncating behaviour
}

void C8::LD_Immediate(uint8_t reg, uint8_t immediate)
{
	V[reg] = immediate;
}

void C8::SNE_Immediate(uint8_t reg, uint8_t val)
{
	if (V[reg] != val)
	{
		PC += 2;
	}
}

void C8::SE_Immediate(uint8_t reg, uint8_t val)
{
	if (V[reg] == val)
	{
		PC += 2;
	}
}

void C8::CALL(uint16_t address)
{
	++SP;
	Stack[SP] = PC;
	PC = address;
}

void C8::JP(uint16_t address)
{
	PC = address;
}

void C8::RET()
{
	PC = Stack[SP];
	--SP;
}

void C8::CLS()
{
	Display.fill(0);
}

void C8::SYS(uint16_t syscall)
{
	// Ignored
}


bool C8::GetDisplay(uint8_t x, uint8_t y)
{
	uint8_t x_byte = x / 8;
	uint8_t x_bit = 8 - (x % 8);

	uint8_t display_byte = Display[x_byte + y * DisplayWidth / 8];
	bool ret = (display_byte >> (x_bit - 1)) & 0x1;
	return ret;
}

bool C8::KeyDown(uint8_t key)
{
	if (key < NumKeys)
	{
		return KeyState[key];
	}
	return false;
}

uint8_t C8::Read8(uint16_t address)
{
	if (address < MemorySize)
	{
		return Memory[address];
	}
	return 0;
}

void C8::Store8(uint16_t address, uint8_t val)
{
	if (address > MemWriteMin && address < MemorySize)
	{
		Memory[address] = val;
	}
}

uint16_t C8::Read16(uint16_t address)
{
	if (address < MemorySize)
	{
		uint16_t hi = Memory[address] << 8;
		uint16_t lo = Memory[address + 1];
		return hi | lo;
	}
	return 0;
}

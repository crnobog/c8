#include "Assembler.h"

using std::string;

const string whitespace = " \t\r\n";

const Assembler::OpcodeData Assembler::Opcodes[] = {
	{ "LD", 0x0, 0x0, 2, 2, &Assembler::Parse_LD },
	{ "JP", 0x0, 0x0, 1, 2, &Assembler::Parse_JP },
	{ "DRW", 0xD000, 0x0, 3, 3, &Assembler::Parse_DRW },
	{ "ADD", 0x0, 0x0, 2, 2, &Assembler::Parse_ADD },
	{ "SNE", 0x0, 0x0, 2, 2, &Assembler::Parse_SNE },
	{ "SE", 0x0, 0x0, 2, 2, &Assembler::Parse_SE },
	{ "CLS", 0x00E0, 0x0, 0, 0, &Assembler::Parse_CLS_RET },
	{ "RET", 0x00EE, 0x0, 0, 0, &Assembler::Parse_CLS_RET },
	{ "SYS", 0x0, 0x0, 1, 1, &Assembler::Parse_SYS_CALL },
	{ "CALL", 0x2000, 0x0, 1, 1, &Assembler::Parse_SYS_CALL },
	{ "OR", 0x8, 0x1, 2, 2, &Assembler::Parse_Arithmetic },
	{ "AND", 0x8, 0x2, 2, 2, &Assembler::Parse_Arithmetic },
	{ "XOR", 0x8, 0x3, 2, 2, &Assembler::Parse_Arithmetic },
	{ "ADD", 0x8, 0x4, 2, 2, &Assembler::Parse_Arithmetic },
	{ "SUB", 0x8, 0x5, 2, 2, &Assembler::Parse_Arithmetic },
	{ "SUBN", 0x8, 0x7, 2, 2, &Assembler::Parse_Arithmetic },
	{ "SHR", 0x8, 0x6, 1, 2, &Assembler::Parse_Shift },
	{ "SHL", 0x8, 0xE, 1, 2, &Assembler::Parse_Shift },
	{ "RND", 0xC, 0x0, 2, 2, &Assembler::Parse_RND },
	{ "SKP", 0xE, 0x9E, 1, 1, &Assembler::Parse_SKP_SKNP },
	{ "SKNP", 0xE, 0xA1, 1, 1, &Assembler::Parse_SKP_SKNP },
};

template< typename T>
bool GetImmediate(const ci_string& operand, T& imm, uint32_t mask)
{
	uint32_t val;
	bool success = sscanf_s(operand.data(), "%x", &val) || sscanf_s(operand.data(), "%d", &val);
	imm = val & mask;
	return success;
}

std::vector<uint8_t> Assembler::GetCode()
{
	return Code;
}

bool Assembler::Assemble(const std::string& program)
{
	uint16_t current_address = 0x200;

	string::size_type current_pos = 0;
	while (current_pos < program.size() && current_pos != string::npos)
	{
		// Skip whitespace
		current_pos = program.find_first_not_of(whitespace, current_pos);
		if (current_pos > program.size() || current_pos == string::npos)
			break;

		// Skip comment
		if (program[current_pos] == '#')
		{
			++current_pos;
			current_pos = program.find_first_of("\n", current_pos) + 1; // Skip newline
		}

		// Now hopefully we have an opcode or label
		auto space_or_colon_idx = program.find_first_of(": \n", current_pos);
		if (space_or_colon_idx == string::npos)
		{
			// Malformed?
			return false;
		}

		switch (program[space_or_colon_idx])
		{
		case ':':
			// Label
		{
			ci_string label = ci_string(&program[current_pos], &program[space_or_colon_idx]);
			auto found = Labels.find(label);
			if (found == end(Labels))
			{
				Labels[label] = current_address;
			}
			else if (found->second == 0x0)
			{
				// Pending label
				Labels[label] = current_address;
				for (uint32_t inst_add : PendingLabels[label])
				{
					uint8_t addr_hi_nibble = (current_address >> 8) & 0xF;
					uint8_t addr_lo_byte = current_address & 0xFF;

					uint8_t hi_byte = Code[inst_add];
					Code[inst_add] = hi_byte | addr_hi_nibble;
					Code[inst_add + 1] = addr_lo_byte;
				}
				PendingLabels.erase(label);
			}
			else
			{
				// Duplicate label, bail
				return false;
			}
			current_pos = space_or_colon_idx + 1;
			break;
		}
		case '\n':
		case ' ':
		{
			// Opcode
			uint16_t inst = 0;
			bool success = ParseInstruction(program, current_pos, space_or_colon_idx, inst, current_pos);
			if (!success)
			{
				return false;
			}
			Code.push_back(inst >> 8);
			Code.push_back(inst & 0xFF);
			current_address += 2;
			break;
		}
		}
	}

	return true;
}

bool Assembler::ParseInstruction(const std::string& program, string::size_type start_pos, string::size_type first_whitespace, uint16_t& encoded, string::size_type& out_pos)
{
	ci_string opcode(&program[start_pos], first_whitespace - start_pos);

	auto found = std::find_if(std::begin(Opcodes), std::end(Opcodes), [&](const OpcodeData& data) { return opcode.compare(data.Name) == 0; });
	if (!found)
	{
		return false;
	}

	const OpcodeData& data = *found;
	std::vector<ci_string> operands;
	operands.reserve(data.MaxOperands);
	string::size_type pos = first_whitespace + 1;
	for (unsigned i = 0; i < data.MaxOperands; ++i)
	{
		// Eat whitespace
		pos = program.find_first_not_of(whitespace, pos);

		// Find end of argument
		string::size_type end = program.find_first_of(", \n\t", pos);

		operands.push_back(ci_string(&program[pos], end - pos));
		pos = end + 1;

		if (program[end] != ',')
			break;
	}
	if (operands.size() < data.MinOperands || operands.size() > data.MaxOperands)
	{
		return false;
	}
	out_pos = pos;

	if (!((*this).*(data.Parser))(data, operands, encoded))
	{
		return false;
	}

	return true;
}

bool Assembler::Parse_SE(const OpcodeData& data, const std::vector<ci_string>& operands, uint16_t& encoded)
{
	uint8_t reg_x;
	if (!IsVRegister(operands[0], reg_x))
		return false;

	uint8_t reg_y;
	uint8_t imm;
	if (IsVRegister(operands[1], reg_y))
	{
		encoded = 0x5000 | reg_x << 8 | reg_y << 4 | 0x0;
		return true;
	}
	else if (Get8BitImmediate(operands[1], imm))
	{
		encoded = 0x3000 | reg_x << 8 | imm;
		return true;
	}

	return false;
}

bool Assembler::Parse_SNE(const OpcodeData& data, const std::vector<ci_string>& operands, uint16_t& encoded)
{
	uint8_t reg_x;
	if (!IsVRegister(operands[0], reg_x))
		return false;

	uint8_t reg_y;
	uint8_t imm;
	if (IsVRegister(operands[1], reg_y))
	{
		encoded = 0x9000 | reg_x << 8 | reg_y << 4 | 0x0;
		return true;
	}
	else if (Get8BitImmediate(operands[1], imm))
	{
		encoded = 0x4000 | reg_x << 8 | imm;
		return true;
	}

	return false;
}

bool Assembler::Parse_ADD(const OpcodeData& data, const std::vector<ci_string>& operands, uint16_t& encoded)
{
	uint8_t target_reg;
	if (IsVRegister(operands[0], target_reg))
	{
		uint8_t source_reg, imm;
		if (IsVRegister(operands[1], source_reg))
		{
			encoded = 0x8000 | target_reg << 8 | source_reg << 4 | 0x4;
			return true;
		}
		else if (Get8BitImmediate(operands[1], imm))
		{
			encoded = 0x7000 | target_reg << 8 | imm;
			return true;
		}
		return false;
	}
	else if (operands[0] == "I")
	{
		uint8_t source_reg;
		if (IsVRegister(operands[1], source_reg))
		{
			encoded = 0xF000 | source_reg << 8 | 0x1E;
			return true;
		}
		return false;
	}

	return false;
}

bool Assembler::Parse_DRW(const OpcodeData& data, const std::vector<ci_string>& operands, uint16_t& encoded)
{
	uint8_t x, y, size;
	if (!IsVRegister(operands[0], x))
		return false;
	if (!IsVRegister(operands[1], y))
		return false;
	if (!Get4BitImmediate(operands[2], size))
		return false;

	encoded = data.OpcodeHi | x << 8 | y << 4 | size;
	return true;
}

bool Assembler::Parse_LD(const OpcodeData& data, const std::vector<ci_string>& operands, uint16_t& encoded)
{
	uint8_t target_reg, source_reg;
	if (IsVRegister(operands[0], target_reg))
	{
		uint8_t imm;
		if (Get8BitImmediate(operands[1], imm))
		{
			encoded = 0x6000 | target_reg << 8 | imm;
			return true;
		}
		else if (IsVRegister(operands[1], source_reg))
		{
			encoded = 0x8000 | target_reg << 8 | source_reg << 4 | 0x0;
			return true;
		}
		else if (operands[1] == "DT")
		{
			encoded = 0xF000 | target_reg << 8 | 0x07;
			return true;
		}
		else if (operands[1] == "K")
		{
			encoded = 0xF000 | target_reg << 8 | 0x0A;
			return true;
		}
		else if (operands[1] == "[I]")
		{
			encoded = 0xF000 | target_reg << 8 | 0x65;
			return true;
		}
		return false;
	}

	if (operands[0] == "I")
	{
		uint16_t imm;
		if (Get12BitImmediate(operands[1], imm))
		{
			encoded = 0xA000 | imm;
			return true;
		}
		return false;
	}

	if (!IsVRegister(operands[1], source_reg))
	{
		return false;
	}

	uint16_t opcode_hi = 0xF000;
	uint16_t opcode_lo = 0x0000;
	if (operands[0] == "F")
	{
		opcode_lo = 0x29;
		return true;
	}
	else if (operands[0] == "DT")
	{
		opcode_lo = 0x15;
		return true;
	}
	else if (operands[0] == "ST")
	{
		opcode_lo = 0x18;
		return true;
	}
	else if (operands[0] == "B")
	{
		opcode_lo = 0x33;
		return true;
	}
	else if (operands[0] == "[I]")
	{
		opcode_lo = 0x55;
		return true;
	}
	else
	{
		return false;
	}

	encoded = opcode_hi | source_reg << 8 | opcode_lo;
	return true;
}

bool Assembler::Parse_JP(const OpcodeData& data, const std::vector<ci_string>& operands, uint16_t& encoded)
{
	uint8_t opcode = 0x0;
	uint8_t base_reg = 0xF;
	uint16_t imm;
	if (operands.size() == 2)
	{
		if (!IsVRegister(operands[0], base_reg))
		{
			return false;
		}
		if (base_reg != 0)
		{
			return false;
		}

		opcode = 0xB;
		if (!Get12BitImmediate(operands.back(), imm))
		{
			return false;
		}
	}
	else
	{
		opcode = 0x1;
		if (!Get12BitImmediate(operands.back(), imm))
		{
			auto found = Labels.find(operands.back());
			if (found == end(Labels))
			{
				// Jump forward
				Labels[operands.back()] = 0x0;
				PendingLabels[operands.back()].push_back(Code.size());
				imm = 0;
			}
			else
			{
				imm = found->second;
			}
		}
	}

	encoded = opcode << 12 | imm;
	return true;
}

bool Assembler::Get4BitImmediate(const ci_string& operand, uint8_t& imm)
{
	return GetImmediate(operand, imm, 0xF);
}

bool Assembler::Get8BitImmediate(const ci_string& operand, uint8_t& imm)
{
	return GetImmediate(operand, imm, 0xFF);
}

bool Assembler::Get12BitImmediate(const ci_string& operand, uint16_t& imm)
{
	return GetImmediate(operand, imm, 0xFFF);
}

bool Assembler::IsVRegister(const ci_string& operand, uint8_t& reg)
{
	if (operand.size() == 2
		&& toupper(operand[0]) == 'V'
		&&	isxdigit(operand[1])
		)
	{
		const char digit = operand[1];
		if (isdigit(digit))
		{
			reg = digit - '0';
		}
		else
		{
			reg = toupper(digit) - 'A' + 0xA;
		}
		return true;
	}
	return false;
}

bool Assembler::Parse_CLS_RET(const OpcodeData& data, const std::vector<ci_string>& operands, uint16_t& encoded)
{
	encoded = data.OpcodeHi;
	return true;
}

bool Assembler::Parse_SYS_CALL(const OpcodeData& data, const std::vector<ci_string>& operands, uint16_t& encoded)
{
	uint16_t imm;
	if (!Get12BitImmediate(operands[0], imm))
	{
		return false;
	}
	encoded = data.OpcodeHi | imm;
	return true;
}

bool Assembler::Parse_Arithmetic(const OpcodeData& data, const std::vector<ci_string>& operands, uint16_t& encoded)
{
	uint8_t target_reg, source_reg;
	if (!IsVRegister(operands[0], target_reg))
	{
		return false;
	}
	if (!IsVRegister(operands[1], source_reg))
	{
		return false;
	}
	encoded = data.OpcodeHi | target_reg << 8 | source_reg << 4 | data.OpcodeLo;
	return true;
}

bool Assembler::Parse_Shift(const OpcodeData& data, const std::vector<ci_string>& operands, uint16_t& encoded)
{
	uint8_t target_reg = 0, source_reg = 0;
	if (!IsVRegister(operands[0], target_reg))
	{
		return false;
	}
	IsVRegister(operands[1], source_reg);
	encoded = data.OpcodeHi | target_reg << 8 | source_reg << 4 | data.OpcodeLo;
	return true;
}

bool Assembler::Parse_RND(const OpcodeData& data, const std::vector<ci_string>& operands, uint16_t& encoded)
{
	uint8_t target_reg, imm;
	if (!IsVRegister(operands[0], target_reg))
	{
		return false;
	}
	if (!Get8BitImmediate(operands[1], imm))
	{
		return false;
	}
	encoded = data.OpcodeHi | target_reg << 8 | imm;
	return true;
}

bool Assembler::Parse_SKP_SKNP(const OpcodeData& data, const std::vector<ci_string>& operands, uint16_t& encoded)
{
	uint8_t reg;
	if (!IsVRegister(operands[0], reg))
	{
		return false;
	}
	encoded = data.OpcodeHi | reg << 8 | data.OpcodeLo;
	return true;
}


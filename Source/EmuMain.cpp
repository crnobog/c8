#include "C8.h"
#include "Assembler.h"
#include "Disassembler.h"

#include "SDL.h"
#include <cstdint>
#include <memory>
#include <array>
#include <vector>
#include <fstream>
#include <iostream>

const int WindowWidth = 640;
const int WindowHeight = 320;

std::string TestProgram =
"LD V0, 0\n"
"LD V1, 0x0\n"
"LD V2, 0\n"
"loophead:\n"
"LD F, V2			# draw character\n"
"DRW V0, V1, 0x5\n"
"ADD V0, 0x5\n"
"ADD V2, 0x1\n"
"SNE V2, 0xF\n"
"JP halt\n"
"SE V0, 0x3C			# 12 chars\n"
"JP loophead\n"
"LD V0, 0\n"
"ADD V1, 0x6			# new line\n"
"JP loophead\n"
"halt:\n"
"JP halt\n"
;

const std::array<uint8_t, 30> TextPrintingExample = {
	{
	0x60, 0x00,
	0x61, 0x00,
	0x62, 0x00,
	0xF2, 0x29,
	0xD0, 0x15,
	0x70, 0x05,
	0x72, 0x01,
	0x42, 0x0F,
	0x12, 0x1C,
	0x30, 0x3C,
	0x12, 0x06,
	0x60, 0x00,
	0x71, 0x06,
	0x12, 0x06,
	0x12, 0x1C
	}
};

int main(int argc, char** argv)
{
	std::vector<uint8_t> rom;

	if (argc < 2)
	{
		//rom.resize(TextPrintingExample.size());
		//std::copy(begin(TextPrintingExample), end(TextPrintingExample), begin(rom));
		Assembler ass;
		if (!ass.Assemble(TestProgram))
		{
			return 1;
		}
		rom = ass.GetCode();

		Disassembler dis;
		dis.Disassemble(rom, stdout);
	}
	else
	{
		const char* rom_filename = argv[1];
		std::ifstream rom_file(rom_filename, std::ios::binary | std::ios::in);
		if (!rom_file)
		{
			return 1;
		}
		rom_file.seekg(0, std::ios_base::end);
		auto length = rom_file.tellg();
		rom_file.seekg(0, std::ios_base::beg);
		if (length)
		{
			rom.resize((unsigned)length);
			rom_file.read((char*)rom.data(), rom.size());
		}
	}

	if (!rom.size())
	{
		return 1;
	}

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		return 1;
	}

	SDL_Window* win = SDL_CreateWindow(
		"C8", 
		100, 100, 
		WindowWidth, WindowHeight,
		SDL_WINDOW_SHOWN
		);
	if (!win)
	{
		return 1;
	}

	SDL_Renderer* ren = SDL_CreateRenderer(
		win, 
		-1,
		SDL_RENDERER_ACCELERATED
		);
	if (!ren)
	{
		return 1;
	}

	C8 cpu(rom);

	SDL_Texture* tex = SDL_CreateTexture(ren, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, cpu.DisplayWidth, cpu.DisplayHeight);
	if (!tex)
	{
		const char* err = SDL_GetError();
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", err, win);
		return 1;
	}

	bool quit = false;
	do
	{
		SDL_Event e;
		while (SDL_PollEvent(&e))
		{
			//If user closes the window
			if (e.type == SDL_QUIT)
				quit = true;
			//If user presses any key
			if (e.type == SDL_KEYDOWN)
				quit = true;
			//If user clicks the mouse
			if (e.type == SDL_MOUSEBUTTONDOWN)
				quit = true;
		}

		cpu.Step();

		const uint32_t on_color = 0xffffffff, off_color = 0xffff;
		uint32_t* pixels = nullptr;
		int pitch = 0;
		SDL_LockTexture(tex, nullptr, (void**)&pixels, &pitch);
		for (int y = 0; y < cpu.DisplayHeight; ++y)
		{
			for (int x = 0; x < cpu.DisplayWidth; ++x)
			{
				pixels[y*cpu.DisplayWidth + x] = cpu.GetDisplay(x, y) ? on_color : off_color;
			}
		}
		SDL_UnlockTexture(tex);

		SDL_RenderClear(ren);
		SDL_RenderCopy(ren, tex, nullptr, nullptr);
		SDL_RenderPresent(ren);
	} while (!quit);

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
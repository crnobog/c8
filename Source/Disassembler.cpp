#include "Disassembler.h"

void Disassembler::Disassemble(const std::vector<uint8_t>& code, FILE* out)
{
	Out = out;
	for (uint32_t i = 0; i < code.size(); i += 2)
	{
		uint16_t inst = code[i] << 8 | code[i + 1];
		Decode(inst);
		fprintf_s(Out, "\n");
	}
	Out = nullptr;
}

void Disassembler::SYS(uint16_t syscall)
{
	fprintf_s(Out, "SYS 0x%x", syscall);
}

void Disassembler::CLS()
{
	fprintf_s(Out, "CLS");
}

void Disassembler::RET()
{
	fprintf_s(Out, "RET");
}

void Disassembler::JP(uint16_t address)
{
	fprintf_s(Out, "JP 0x%x", address);
}

void Disassembler::CALL(uint16_t address)
{
	fprintf_s(Out, "CALL 0x%x", address);
}

void Disassembler::LD_Immediate(uint8_t reg, uint8_t immediate)
{
	fprintf_s(Out, "LD V%x, 0x%x", reg, immediate);
}

void Disassembler::ADD_Immediate(uint8_t reg, uint8_t immediate)
{
	fprintf_s(Out, "ADD V%x, 0x%x", reg, immediate);
}

void Disassembler::LD_Address(uint16_t immediate)
{
	fprintf_s(Out, "LD I, 0x%x", immediate);
}

void Disassembler::JP_V0(uint16_t rel_address)
{
	fprintf_s(Out, "JP V0, 0x%x", rel_address);
}

void Disassembler::SE_Immediate(uint8_t reg, uint8_t val)
{
	fprintf_s(Out, "SE V%x, 0x%x", reg, val);
}

void Disassembler::SNE_Immediate(uint8_t reg, uint8_t val)
{
	fprintf_s(Out, "SNE V%x, 0x%x", reg, val);
}

void Disassembler::SE_Reg(uint8_t reg_x, uint8_t reg_y)
{
	fprintf_s(Out, "SE V%x, V%x", reg_x, reg_y);
}

void Disassembler::SNE_Reg(uint8_t reg_x, uint8_t reg_y)
{
	fprintf_s(Out, "SNE V%x, V%x", reg_x, reg_y);
}

void Disassembler::LD_Vx_Vy(uint8_t reg_x, uint8_t reg_y)
{
	fprintf_s(Out, "LD V%x, V%x", reg_x, reg_y);
}

void Disassembler::OR(uint8_t reg_x, uint8_t reg_y)
{
	fprintf_s(Out, "OR V%x, V%x", reg_x, reg_y);
}

void Disassembler::AND(uint8_t reg_x, uint8_t reg_y)
{
	fprintf_s(Out, "AND V%x, V%x", reg_x, reg_y);
}

void Disassembler::XOR(uint8_t reg_x, uint8_t reg_y)
{
	fprintf_s(Out, "XOR V%x, V%x", reg_x, reg_y);
}

void Disassembler::ADD(uint8_t reg_x, uint8_t reg_y)
{
	fprintf_s(Out, "ADD V%x, V%x", reg_x, reg_y);
}

void Disassembler::SUB(uint8_t reg_x, uint8_t reg_y)
{
	fprintf_s(Out, "SUB V%x, V%x", reg_x, reg_y);
}

void Disassembler::SHR(uint8_t reg_x, uint8_t reg_y)
{
	fprintf_s(Out, "SHR V%x, V%x", reg_x, reg_y);
}

void Disassembler::SUBN(uint8_t reg_x, uint8_t reg_y)
{
	fprintf_s(Out, "SUBN V%x, V%x", reg_x, reg_y);
}

void Disassembler::SHL(uint8_t reg_x, uint8_t reg_y)
{
	fprintf_s(Out, "SHL V%x, V%x", reg_x, reg_y);
}

void Disassembler::SKP(uint8_t reg)
{
	fprintf_s(Out, "SKP V%x", reg);
}

void Disassembler::SKNP(uint8_t reg)
{
	fprintf_s(Out, "SKNP V%x", reg);
}

void Disassembler::LD_Vx_DT(uint8_t reg)
{
	fprintf_s(Out, "LD V%x, DT", reg);
}

void Disassembler::LD_Vx_K(uint8_t reg)
{
	fprintf_s(Out, "LD V%x, K", reg);
}

void Disassembler::LD_DT_Vx(uint8_t reg)
{
	fprintf_s(Out, "LD DT, V%x", reg);
}

void Disassembler::LD_ST_Vx(uint8_t reg)
{
	fprintf_s(Out, "LD ST, V%x", reg);
}

void Disassembler::ADD_I_Vx(uint8_t reg)
{
	fprintf_s(Out, "ADD I, V%x", reg);
}

void Disassembler::LD_F_Vx(uint8_t reg)
{
	fprintf_s(Out, "LD F, V%x", reg);
}

void Disassembler::LD_B_Vx(uint8_t reg)
{
	fprintf_s(Out, "LD B, V%x", reg);
}

void Disassembler::LD_I_Vx(uint8_t reg)
{
	fprintf_s(Out, "LD [I], V%x", reg);
}

void Disassembler::LD_Vx_I(uint8_t reg)
{
	fprintf_s(Out, "LD V%x, [I]", reg);
}

void Disassembler::RND(uint8_t reg, uint8_t mask)
{
	fprintf_s(Out, "RND V%x, 0x%x", reg, mask);
}

void Disassembler::DRW(uint8_t reg_x, uint8_t reg_y, uint8_t sprite_size)
{
	fprintf_s(Out, "DRW V%x, V%x, 0x%x", reg_x, reg_y, sprite_size);
}

#pragma once

#include <cstdint>

template<typename T>
struct C8Decoder
{
	T* AsT() 
	{
		return static_cast<T*>(this);
	}

	void Decode_ClsRetSys(uint16_t nnn)
	{
		switch(nnn & 0xFF)
		{
		case 0xE0: AsT()->CLS();	break;
		case 0xEE: AsT()->RET();	break;
		default:   AsT()->SYS(nnn);	break;
		}
	}

	void Decode_RegRegArithmetic(uint8_t target_reg, uint8_t source_reg, uint8_t operation)
	{
		switch (operation)
		{
		case 0x0: AsT()->LD_Vx_Vy(target_reg, source_reg);	break;
		case 0x1: AsT()->OR(target_reg, source_reg);		break;
		case 0x2: AsT()->AND(target_reg, source_reg);		break;
		case 0x3: AsT()->XOR(target_reg, source_reg);		break;
		case 0x4: AsT()->ADD(target_reg, source_reg);		break;
		case 0x5: AsT()->SUB(target_reg, source_reg);		break;
		case 0x6: AsT()->SHR(target_reg, source_reg);		break;
		case 0x7: AsT()->SUBN(target_reg, source_reg);		break;
		case 0xE: AsT()->SHL(target_reg, source_reg);		break;
		}
	}

	void Decode_LD_ADD_Spec(uint8_t reg, uint8_t kk)
	{
		switch (kk)
		{
		case 0x7:	AsT()->LD_Vx_DT(reg);	break;
		case 0xA:	AsT()->LD_Vx_K(reg);	break;
		case 0x15:	AsT()->LD_DT_Vx(reg);	break;
		case 0x18:	AsT()->LD_ST_Vx(reg);	break;
		case 0x1E:	AsT()->ADD_I_Vx(reg);	break;
		case 0x29:	AsT()->LD_F_Vx(reg);	break;
		case 0x33:	AsT()->LD_B_Vx(reg);	break;
		case 0x55:	AsT()->LD_I_Vx(reg);	break;
		case 0x65:	AsT()->LD_Vx_I(reg);	break;
		default:
			// Fail
			break;
		}
	}

	void Decode_SKP_SKNP(uint8_t reg, uint8_t kk)
	{
		switch (kk)
		{
		case 0x9E:	AsT()->SKP(reg);	break;
		case 0xA1:	AsT()->SKNP(reg);	break;
		}
		// Fail
	}

	void Decode(uint16_t inst)
	{
		// Target register is always second most significant nibble
		const uint8_t x = (inst >> 8) & 0xF;
		// Source register is always third most significant nibble
		const uint8_t y = (inst >> 4) & 0xF;
		// 12-bit constant
		const uint16_t nnn = inst & 0xFFF;
		// one-byte constant
		const uint8_t kk = inst & 0xFF;
		// one-nibble constant
		const uint8_t n = inst & 0xF;

		// Opcode is highest nibble
		switch ((inst >> 12) & 0xF)
		{
		// Two-stage decodes:
		case 0x0: Decode_ClsRetSys(nnn);			break;
		case 0x8: Decode_RegRegArithmetic(x, y, n);	break;
		case 0xE: Decode_SKP_SKNP(x, kk);			break;
		case 0xF: Decode_LD_ADD_Spec(x, kk);		break;
		case 0x9:
			if((inst & 0xF) == 0)
			{
				AsT()->SNE_Reg(x, y);
			}
			break;

		// Handled immediately:
		case 0x1: AsT()->JP(nnn);					break;
		case 0x2: AsT()->CALL(nnn);					break;
		case 0x3: AsT()->SE_Immediate( x, kk );		break;
		case 0x4: AsT()->SNE_Immediate( x, kk );	break;
		case 0x5: AsT()->SE_Reg(x, y);				break;
		case 0x6: AsT()->LD_Immediate(x, kk);		break;
		case 0x7: AsT()->ADD_Immediate(x, kk);		break;
		case 0xA: AsT()->LD_Address(nnn);			break;
		case 0xB: AsT()->JP_V0(nnn);				break;
		case 0xC: AsT()->RND(x, kk);				break;
		case 0xD: AsT()->DRW(x, y, n);				break;
		}
	}
};
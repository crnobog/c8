#pragma once

#include <string>
#include <hash_map>
#include <cstdint>
#include <algorithm>
#include <vector>

struct CharTraits_CaseInsensitive : public std::char_traits<char>
{
	static bool eq(char c1, char c2)
	{
		return toupper(c1) == toupper(c2);
	}

	static bool ne(char c1, char c2)
	{
		return toupper(c1) != toupper(c2);
	}

	static bool lt(char c1, char c2)
	{
		return toupper(c1) < toupper(c2);
	}

	static int compare(const char* s1, const char* s2, size_t n)
	{
		return _memicmp(s1, s2, n);
	}

	static const char* find(const char* s, int n, char a)
	{
		while (n-- > 0 && toupper(*s) != toupper(a)) {
			++s;
		}
		return s;
	}
};

typedef std::basic_string<char, CharTraits_CaseInsensitive> ci_string;

class Assembler
{
#define PARSER_ARGS const OpcodeData&, const std::vector<ci_string>&, uint16_t&
	struct OpcodeData
	{
		const char* Name;
		uint16_t	OpcodeHi;
		uint16_t	OpcodeLo;
		unsigned	MinOperands;
		unsigned	MaxOperands;
		bool (Assembler::*Parser)(PARSER_ARGS);
	};
	std::hash_map<ci_string, uint16_t>	Labels;
	std::vector<uint8_t>				Code;
	std::hash_map<ci_string, std::vector<uint16_t>> PendingLabels;

	static const unsigned NumOpcodes = 21;
	static const OpcodeData Opcodes[NumOpcodes];

	bool IsVRegister(const ci_string& operand, uint8_t& reg);
	bool Get12BitImmediate(const ci_string& operand, uint16_t& imm);
	bool Get8BitImmediate(const ci_string& operand, uint8_t& imm);
	bool Get4BitImmediate(const ci_string& operand, uint8_t& imm);

	bool Parse_JP(PARSER_ARGS);
	bool Parse_LD(PARSER_ARGS);
	bool Parse_DRW(PARSER_ARGS);
	bool Parse_ADD(PARSER_ARGS);
	bool Parse_SNE(PARSER_ARGS);
	bool Parse_SE(PARSER_ARGS);
	bool Parse_CLS_RET(PARSER_ARGS);
	bool Parse_SYS_CALL(PARSER_ARGS);
	bool Parse_Arithmetic(PARSER_ARGS);
	bool Parse_Shift(PARSER_ARGS);
	bool Parse_RND(PARSER_ARGS);
	bool Parse_SKP_SKNP(PARSER_ARGS);
#undef PARSER_ARGS

	bool ParseInstruction(const std::string& program, std::string::size_type start_pos, std::string::size_type first_whitespace, uint16_t& encoded, std::string::size_type& out_pos);

public:
	bool Assemble(const std::string& program);

	std::vector<uint8_t> GetCode();
};
